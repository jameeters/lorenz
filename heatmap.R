library('scatterplot3d')
library('rgl')
cframe = read.csv("lorenz_heatmap1030301.csv")
colrange = colorRampPalette(c('blue', 'red', 'orange', 'yellow'))
countrange = max(cframe$count) - min(cframe$count)
colors = colrange(countrange)

counts = c(seq(min(cframe$count), max(cframe$count)))

usecolors = c()
for (k in cframe$count) {
  usecolors = c(usecolors, colors[which(counts == k)])
}

scatterplot3d(x=cframe$x, y=cframe$y, z=cframe$z, color=usecolors, xlab='x', ylab='y', zlab='z')
plot3d(x=cframe$x, y=cframe$y, z=cframe$z, col=usecolors, xlab='x', ylab='y', zlab='z')