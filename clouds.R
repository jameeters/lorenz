library('dplyr')
library('scatterplot3d')
library(stringr)

frame = read.csv("lorenz30000.csv")

# I want to heatmap the state space.

obsmins = c(min(frame$x), min(frame$y), min(frame$z))
obsmaxs = c(max(frame$x), max(frame$y), max(frame$z))

boxmins = c(-100, -100, -100)
boxmaxs = c(100, 100, 100)
boxsz = 2

# centerpoint coordinates
cx = c()
cy = c()
cz = c()
count = c()
for (x0 in seq(boxmins[1], boxmaxs[1], by=boxsz)) {
  x1 = x0 + boxsz
  for (y0 in seq(boxmins[2], boxmaxs[2], by=boxsz)) {
    y1 = y0 + boxsz
    for (z0 in seq(boxmins[3], boxmaxs[3], by=boxsz)) {
      z1 = z0 + boxsz
      cx = c(cx, mean(c(x0, x1)))
      cy = c(cy, mean(c(y0, y1)))
      cz = c(cz, mean(c(z0, z1)))
      count =c(count, nrow(
        frame %>% 
          filter(x0 < x & x <= x1) %>%
          filter(y0 < y & y <= y1) %>%
          filter(z0 < z & z <= z1)
      ))
      if(length(count) %% 100 == 0) {
        print(length(count))
      }
    }
  }
}
n = length(count)
cframe = data.frame(x=cx, y=cy, z=cz, count=count) %>% filter(count > 0)
write.csv(cframe, str_glue('lorenz_heatmap{n}.csv'), quote=F, row.names = F)